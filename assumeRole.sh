#!/usr/bin/env bash

PROFILE=$1
ROLE_ARN=$2

RE=`aws sts assume-role --role-arn ${ROLE_ARN} --role-session-name ${PROFILE} --duration-seconds 14400 --profile \
                                                                                                        ${PROFILE}`
if [[ $? != 0 ]]; then
    echo "[ERROR] Something is wrong, please check your credential again or contact Network team."
    exit 0
fi

AWS_ACCESS_KEY_ID=`echo ${RE} | jq -r ".Credentials.AccessKeyId"`
AWS_SECRET_ACCESS_KEY=`echo ${RE} | jq -r ".Credentials.SecretAccessKey"`
AWS_SESSION_TOKEN=`echo ${RE} | jq -r ".Credentials.SessionToken"`

BAP_PATH="$HOME/.baptools/.aws/"
if [[ ! -d ${BAP_PATH} ]]; then
    mkdir -p ${BAP_PATH}
fi

echo "export AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}" > ${BAP_PATH}cacheCredentialRole
echo "export AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}" >> ${BAP_PATH}cacheCredentialRole
echo "export AWS_SESSION_TOKEN=${AWS_SESSION_TOKEN}" >> ${BAP_PATH}cacheCredentialRole

source ${BAP_PATH}cacheCredentialRole