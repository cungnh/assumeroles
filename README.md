This shell can help you assume a role and export the credential of AWS into env.

# Roles ARN
Roles are the object include policies that would provide permission to access AWS resources. For example:
S3, SES, SQS,...

It depends on the project and environment of the project. Team Network will serve an ARN string to developers.
Developers will use that role string to assume the role. Then get token to access AWS resources which are necessary.
The role string will have this format as the example below:

```text
arn:aws:iam::435730666777:role/testAssumeRole
```

## Dependencies 
    - awscli
    - jq

## Compatible
    - Ubuntu
    - Mac OS

## Install

### Install awscli
For Linux OS (Ubuntu)
```bash
sudo apt-get install -y awscli
```

For Mac OS
```textmate
Following link:
https://docs.aws.amazon.com/cli/latest/userguide/install-macos.html
```

### Setup awscli tool
Each project (Ex: Ezpay, EMO,...etc), it depends on the requirement of the project, each developer will be provided a
credential on AWS by the Network team. For example:

>>
- AWS AccessId: AKIAWK44SK**********
- AWS SecretAccessKey: irvU5iZZjpyQLoT8HzrqjAd***************
>>

After installed awscli tool, we'll setup the credential. Example, we want to setup the credential for Ezpay project.
 
In the file ~/.aws/credentials, we'll add a block, for example:
```ini
[Ezpay]
aws_access_key_id = AKIAWK44SK**********
aws_secret_access_key = irvU5iZZjpyQLoT8HzrqjAd***************
```

In the file ~/.aws/config, we'll add two block, for example:
```ini
[profile Ezpay]
region = ap-southeast-1
output = json
```

Then, we will run the shell "assumeRole.sh" with arguments. The format is:
```text
./assumeRole.sh <profile_name> <arn_string>
```

For example:
```bash
./assumeleRole.sh  Ezpay  arn:aws:iam::435730666777:role/testAssumeRole
```
